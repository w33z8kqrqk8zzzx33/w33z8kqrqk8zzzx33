async function getproblem(pid) {
    let result = await fetch("https://www.luogu.com.cn/problem/"+pid,{headers:{"x-luogu-type":"content-only"}});
    let jsval = await result.json();
    return jsval["currentData"]["problem"];
}
async function renderproblem(pid) {
    let prbl = await getproblem(pid);
    var resp = "";
    if(prbl["background"]) {
        resp += "## 题目背景\n";
        resp += prbl["background"] + "\n";
    }
    resp += "## 题目描述\n";
    resp += prbl["description"] + "\n";
    resp += "## 输入格式\n";
    resp += prbl["inputFormat"] + "\n";
    resp += "## 输出格式\n";
    resp += prbl["outputFormat"] + "\n";
    var idx = 0;
    resp += "## 输入输出样例\n"
    for(idx=0;idx<prbl["samples"].length;idx++) {
        resp += "### 输入 #" + (idx+1) + "\n";
        resp += "```\n" + prbl["samples"][idx][0] + "\n```\n";
        resp += "### 输出 #" + (idx+1) + "\n";
        resp += "```\n" + prbl["samples"][idx][1] + "\n```\n";
    }
    resp += "## 说明/提示\n"
    resp += prbl["hint"] + "\n";
    console.log(resp);
}
async function renderproblem_pico(pid) {
    let prbl = await getproblem(pid);
    var resp = "---\n";
    resp += "Title: " + prbl["pid"] + " " + prbl["title"] + "\n";
    resp += "Author: w33z8kqrqk8zzzx33\n";
    resp += "Date: 1970-01-01\n";
    resp += "Template: index\n---\n\n";
    if(prbl["background"]) {
        resp += "## 题目背景\n";
        resp += prbl["background"] + "\n\n";
    }
    resp += "## 题目描述\n";
    resp += prbl["description"] + "\n\n";
    resp += "## 输入格式\n";
    resp += prbl["inputFormat"] + "\n\n";
    resp += "## 输出格式\n";
    resp += prbl["outputFormat"] + "\n\n";
    var idx = 0;
    resp += "## 输入输出样例\n\n"
    for(idx=0;idx<prbl["samples"].length;idx++) {
        resp += "### 输入 #" + (idx+1) + "\n";
        resp += "```\n" + prbl["samples"][idx][0] + "\n```\n\n";
        resp += "### 输出 #" + (idx+1) + "\n";
        resp += "```\n" + prbl["samples"][idx][1] + "\n```\n\n";
    }
    resp += "## 说明/提示\n"
    resp += prbl["hint"] + "\n\n";
    console.log(resp);
}
async function getblogcontent(bid) {
    let result = await fetch("https://www.luogu.com.cn/api/blog/detail/"+bid,{headers:{"x-luogu-type":"content-only"}});
    let jsval = await result.json();
    return jsval["data"];
}
async function renderblog(bid) {
    let z = await getblogcontent(bid);
    var resp = "";
    resp += "# " + z["Title"] + "\n";
    resp += z["Identifier"] + " [" + z["Type"] + "]  \n";
    resp += "Published: " + new Date(z["PostTime"]*1000).toString() + "  \n";
    resp += "\n---\n";
    resp += z["Content"];
    console.log(resp);
}